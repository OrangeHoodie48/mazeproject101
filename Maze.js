class UnweightedGraph{
	constructor(){
		this.nodes = new Map(); 
	}
		
	addPath(wall){
		for(let cell of wall.touchingCells){
			if(!this.nodes.has(cell)){
				this.nodes.set(cell, []); 
			}
			for(let cell2 of wall.touchingCells){
				if(cell != cell2){
					if(!this.nodes.get(cell).includes(cell2)){
						this.nodes.get(cell).push(cell2);
					}
				}
			} 
		}
	}
		
}

class Wall {
	constructor(){
		this.wallExists = true; 
		this.touchingCells = []; 
	}
	
	addCell(cell){
		this.touchingCells.push(cell); 
	}

	get exists(){
		return this.wallExists; 	
	}

	set exists(value){
		this.wallExists = value; 
	}

	static getNeighbor(cell, wall){
		for(let touchingCell of wall.touchingCells){
			if(touchingCell != cell){
				return touchingCell; 
			}
		}	
	}
}


class Cell { 

	constructor(x, y,walls){
		this.x = x; 
		this.y = y; 
		this.northWall = walls.northWall;
		this.northWall.addCell(this);

		this.eastWall = walls.eastWall;
		this.eastWall.addCell(this); 

		this.southWall = walls.southWall; 
		this.southWall.addCell(this);

		this.westWall = walls.westWall;
		this.westWall.addCell(this); 

		this.northEdge = false; 
		this.eastEdge = false; 
		this.southEdge = false; 
		this.westEdge = false;

		this.visited = false; 

	}

	hasWalls(){
		if(this.northWall == false && this.southWall == false && this.eastWall == false && this.westWall == false){
			return false;
		}
		return true;
	}
	
	removeWall(wall, removedWalls){
		if(wall.toUpperCase() === "NORTH"){
			this.northWall.exists = false;
			removedWalls.add(this.northWall);
		}
		else if(wall.toUpperCase() === "EAST"){
			this.eastWall.exists = false;
			removedWalls.add(this.eastWall); 
		}
		else if(wall.toUpperCase() === "SOUTH"){
			this.southWall.exists = false;
			removedWalls.add(this.southWall);
		}
		else if(wall.toUpperCase() === "WEST"){
			this.westWall.exists = false;
			removedWalls.add(this.westWall); 
		}
		else{
			throw new Error("input error. Problems was walls"); 
		}
	}

	getWalls(width){
		let cellData = "cell 1: "; 
		if(this.hasWalls() == true){
			let walls = {points: []}; 
			if(this.northWall.exists == true){ 
				walls.points.push({x1: this.x, y1: this.y, x2: this.x + width, y2: this.y});
			}
			if(this.eastWall.exists == true){ 
				walls.points.push({x1: this.x + width, y1: this.y, x2: this.x + width, y2: this.y + width});
			}
			if(this.southWall.exists == true){
				walls.points.push({x1: this.x, y1: this.y + width, x2: this.x + width, y2: this.y + width});
			}
			if(this.westWall.exists == true){ 
				walls.points.push({x1: this.x, y1: this.y, x2: this.x, y2: this.y + width});
			}
			return walls; 
		}
		return null; 
	}


	static createCell(x, y, walls, edge1, edge2){

		if((edge1.toUpperCase() === edge2.toUpperCase()) && edge1.toUpperCase() !== "FALSE"){
			throw new Error("Two edges of a cell may not be identical"); 
		}
		let cell = new Cell(x, y, walls); 

		if(edge1.toUpperCase() === "NORTH"){
			cell.northEdge = true; 
		}
		else if(edge1.toUpperCase() === "EAST"){
			cell.eastEdge = true;
		}
		else if(edge1.toUpperCase() === "SOUTH"){
			cell.southEdge = true;
		}
		else if(edge1.toUpperCase() === "WEST"){
			cell.westEdge = true; 
		} 
		else if(edge1.toUpperCase() !== "FALSE"){
			throw new Error("Edge creation error"); 
		}
		
		if(edge2.toUpperCase() === "NORTH"){
			cell.northEdge = true; 
		}
		else if(edge2.toUpperCase() === "EAST"){
			cell.eastEdge = true;
		}
		else if(edge2.toUpperCase() === "SOUTH"){
			cell.southEdge = true;
		}
		else if(edge2.toUpperCase() === "WEST"){
			cell.westEdge = true; 
		} 
		else if(edge2.toUpperCase() !== "FALSE"){
			throw new Error("Edge creation error"); 
		}
	
		return cell;
	}

}

class Maze{
	constructor(ctx, x, y, rows, cols, cellWidth){	
		this.ctx = ctx;
		this.x = x; 
		this.y = y; 
		this.rows = rows; 
		this.cols = cols;
		this.cellWidth = cellWidth; 
		this.cells = []; 
		this.cellRows = []; 
		this.createCells(); 
		this.removedWalls = new Set(); 
		this.graph = null; 
		this.entrance = null; 	
		this.exit = null; 
		this.cellsVisited = 0; 
		
	}

	createUnweightedGraph(){
		this.graph = new UnweightedGraph();
		for(let wall of this.removedWalls){
			this.graph.addPath(wall);
		} 
	}
	
	isHeads(){ 
		let num = Math.floor(Math.random() * 2) + 1; 
		if(num == 2){
			return true; 
		} 
		return false; 
	}
	
	//takes num and returns a random cell along the assoicated outer edge. 
	//0: North, 1: East, 2: South, 3: West
	randomCellOnSide(sideNum){
		if(sideNum == 0){
			return this.cellRows[0][Math.floor(Math.random() * this.cols)]; 
		}
		else if(sideNum == 1 || sideNum == 3){
			let row = Math.floor(Math.random() * this.rows);
			if(sideNum == 1){
				return this.cellRows[row][this.cols -1]; 
			}
			else{
				return this.cellRows[row][0]; 
			} 
		}
		else if(sideNum == 2){
			return this.cellRows[this.rows - 1][Math.floor(Math.random() * this.cols)];
		}
		else{
			throw new Error("Invalid input. sideNum was " + sideNum); 
		}
	}
	addEntranceExit(){

	 	//will add random entrance and exits
		/*
		let entranceSide = Math.floor(Math.random() * 4);
		let exitSide = Math.floor(Math.random() * 4); 
		while(exitSide == entranceSide){
			exitSide = Math.floor(Math.random() * 4); 
		}
		
		//this.entrance = this.randomCellOnSide(3); 
		//this.exit = this.randomCellOnSide(1); 
		
		*/

		this.entrance = this.cellRows[this.rows - 1][0]; 
		this.exit = this.cellRows[Math.floor((this.rows - 1) / 2)][this.cols - 1];
		this.ctx.font = '25pt Arial'; 
		
		this.ctx.fillStyle = 'white'; 
		this.highlightCell(this.entrance); 
		this.ctx.fillText('Entrance', 50, this.y + this.rows * this.cellWidth + 35);

		this.ctx.fillStyle = 'black'; 
		this.highlightCell(this.exit); 		
		this.ctx.fillText('Exit', this.cols * this.cellWidth - this.ctx.measureText('Exit').width, this.y + this.rows * this.cellWidth + 35)
	}
	
	//heads == true remove north wall. false remove east wall.
	sidewinderAlgorithm(){
		let run; 
		let heads; 
		for(let row of this.cellRows){
			run = []; 
			for(let col = 0; col < this.cols; col++){
				if(row[col].eastEdge == true){
					if(row[col].northEdge == false){
						run.push(row[col]); 
						run[Math.floor(Math.random() * run.length)].removeWall("NORTH", this.removedWalls);
						run = [];
					}

				}
				else if(row[col].northEdge == true){
					row[col].removeWall("EAST", this.removedWalls); 
					run.push(row[col]); 
				}	
				else{
					heads = this.isHeads();
					if(heads == true){
						run.push(row[col]); 
						run[Math.floor(Math.random() * run.length)].removeWall("NORTH", this.removedWalls);
						run = []; 
					}
					else{	
						row[col].removeWall("EAST", this.removedWalls); 
						run.push(row[col]); 
					}
				}
			}
		}
	}
	
	binaryTreeAlgorithm(){
		let heads;
		for(let cell of this.cells){ 
			if(cell.northEdge == true){
				if(cell.eastEdge == false){
					cell.removeWall("EAST", this.removedWalls);  
				} 
			}
			else if(cell.eastEdge == true){
				cell.removeWall("NORTH", this.removedWalls);  
			}
			else{
				heads = this.isHeads(); 
				if(heads == true){
					cell.removeWall("NORTH", this.removedWalls);  
				}
				else{	
					cell.removeWall("EAST", this.removedWalls);  
				}
			}
			  
		}
	}

	createCells(){
		let edge1 = "false"; 
		let cell; 
		let x; 
		let y = this.y; 
		let edge2 = "FALSE"; 
		let walls; 
		let northWall; 
		let southWall; 
		let eastWall; 
		let westWall;

		for(let row = 0; row < this.rows; row++){
			this.cellRows.push([]); 
			if(row == 0){
				edge1 = 'NORTH';  
			}
			else if(row == this.rows -1){
				edge1 = 'SOUTH';  
			}
			else{
				edge1 = 'FALSE'; 
			}
			x = this.x; 

			for(let col = 0; col < this.cols; col++){
				if(col == 0){
					edge2 = "WEST";
					westWall = new Wall(); 
					eastWall = new Wall(); 
				}				
				else if(col == this.cols -1){
					edge2 = "EAST";
					westWall = this.cellRows[row][col - 1].eastWall; 
					eastWall = new Wall(); 
				}
				else{
					edge2 = "FALSE";
					eastWall = new Wall(); 
					westWall = this.cellRows[row][col -1].eastWall;  
				}
				
				if(edge1 === "NORTH") { 
					northWall = new Wall();
					southWall = new Wall();  
				}
				else if(edge1 === "SOUTH"){ 
					southWall = new Wall(); 	
					northWall = this.cellRows[row - 1][col].southWall; 
				}
				else{
					southWall = new Wall(); 
					northWall = this.cellRows[row-1][col].southWall; 
				}
				

				walls = {northWall, eastWall, southWall, westWall}; 
				cell = Cell.createCell(x, y, walls, edge1, edge2);

				this.cellRows[row][col] = cell;
				this.cells.push(cell);
				x += this.cellWidth; 	
			}
			y += this.cellWidth; 
		}
  	}

		drawMaze(){
			for(let cell of this.cells){  
				let walls = cell.getWalls(this.cellWidth); 
				if(walls != null){  
					for(let wall of walls.points){	
						ctx.beginPath();
						ctx.moveTo(wall["x1"], wall["y1"]); 
						ctx.lineTo(wall["x2"], wall["y2"]);	
						ctx.closePath(); 	
						ctx.stroke();
					}
				}
				 
			}
		}

		highlightCell(cell){
			this.ctx.fillRect(cell.x, cell.y, this.cellWidth, this.cellWidth);
		}	
		


		drawPath(startCell, endCell){

			function getPath(maze, cell, history){
				if(cell == endCell){
					return history; 
				} 

				let connectedCells = maze.graph.nodes.get(cell); 
				if(connectedCells.every(r => history.includes(r))){
					return 'false'; 
				}
				
				let results;
				for(let connectedCell of connectedCells){
					if(history.includes(connectedCell)){
						continue;
					} 
					results = getPath(maze, connectedCell, history.concat(connectedCell)); 
					if(results !== 'false'){
						return results; 
					}
				}
				return 'false'; 
			}

			this.ctx.fillStyle = 'black'; 
			let path = getPath(this, startCell, [startCell]); 
			for(let cell of path){  
				this.highlightCell(cell); 
			} 
				
		}

		getRandomNeighborCell(cell){
			let neighbors = []; 
			if(cell.northEdge == false){
				neighbors.push(0); 
			}
			if(cell.eastEdge == false){
				neighbors.push(1);
			}
			if(cell.southEdge == false){
				neighbors.push(2); 
			}
			if(cell.westEdge == false){
				neighbors.push(3); 
			}

			let neighborNum = neighbors[Math.floor(Math.random() * neighbors.length)];
			let wall;
			let wallName;
			if(neighborNum == 0){
				wall = cell.northWall; 
				wallName = "NORTH"; 
			}
			else if(neighborNum == 1){
				wall = cell.eastWall; 
				wallName = "EAST"; 
			}
			else if(neighborNum == 2){
				wall = cell.southWall; 
				wallName = "SOUTH"; 
			}
			else if(neighborNum == 3){
				wall = cell.westWall; 
				wallName = "WEST"; 
			}
			else{
				throw new Error("Some how we got an invalid neighborNum");
			}
				
			return {cell: Wall.getNeighbor(cell, wall), wall: wallName};
			 
		}		

		aldousBroderAlgorithm(){
			let cell = this.cells[0]; 
			cell.visited = true;
			this.cellsVisited++;
			let previousCell; 
			let wall; 
			let returnObj; 
			
			while(this.cellsVisited < this.cells.length){
				if(cell.visited == true){
					previousCell = cell;
					returnObj = this.getRandomNeighborCell(cell); 
					cell = returnObj.cell;
					wall = returnObj.wall;


					
				}
				else{
					
				
		
			

					cell.visited = true; 
					this.cellsVisited++; 
					previousCell.removeWall(wall, this.removedWalls);
		 
					previousCell = cell;
					returnObj = this.getRandomNeighborCell(cell); 	
					cell = returnObj.cell; 
					wall = returnObj.wall; 	
					
				}
				
			}


		}

}